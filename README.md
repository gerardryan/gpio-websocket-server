# README #

### To Install ###

**Install required libraries**
```
#!bash
sudo apt-get install wiringpi
```
```
#!bash
sudo apt-get install libssl-dev
```


**Update system**
```
#!bash
sudo apt-get update
```
```
#!bash
sudo apt-get upgrade
```


**Run the build file**
```
#!bash
./build
```


**Run the server**
```
#!bash
sudo ./gpioServer -pmo 1 1
```


**For CLI help**
```
#!bash
sudo ./gpioServer -h
```


### What is this repository for? ###
This a simple server designed to allow remote communication to the GPIO pins on a raspberry pi via the websockets protocol aka from the web browser.

### Websockets command specification ###
* This implementation follows the wiringPi GPIO pin numbering as shown [here](https://projects.drogon.net/raspberry-pi/wiringpi/pins/).
* For setting up a websocket connection see */www_example/index.html* or go [here](https://developer.mozilla.org/en-US/docs/Web/API/WebSockets_API/Writing_WebSocket_client_applications).
* The current implementation takes messages/commands in the form "*PIN ACTION VALUE*" where PIN is the [wiringPi pin number](https://projects.drogon.net/raspberry-pi/wiringpi/pins/), *ACTION* is the action to preform on the given pin and *VALUE* is the value for the *ACTION* currently only available on *WRITE*.
* Pins numbers currently available range from "0" to "20".
* Actions currently include "*PIN READ*", "*PIN WRITE VALUE*" and "*PIN TOGGLE*". 
* Actions also have corresponding integer values that are as follows: "*READ*" = "*1*", "WRITE" = "*2*" and "*TOGGLE*" = "*3*".
* Values currently available (for write action) are "*PIN WRITE HIGH*" and "*PIN WRITE LOW*" with "*PIN WRITE 1*" and "*PIN WRITE 0*" their integer counterparts.
* The response form a *READ* action will be in the form "*PIN VALUE*" where *VALUE* is returned as an integer eg. 1 or 0.

### CLI help ###
Usage: `sudo ./gpioServer [OPTIONS]`

Example: `sudo ./gpioServer -p 12345 -t 10 -pmo 1 1`

| Option                        | Description                                                                                                                                                                                                    | Alt Option     |
| ----------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------- |
| `--help`                      | Displays help message.                                                                                                                                                                                         | `-h`           | 
| `--port NUM`                  | Port for the server to listen on.                                                                                                                                                                              | `-p NUM`       |
| `--backlog NUM`               | The length of the backlog of unhandled connection.                                                                                                                                                             | `-b NUM`       |
| `--threads NUM`               | Number of threads to used to process connections.                                                                                                                                                              | `-t NUM`       |
| `--buffer-size NUM`           | Buffer size for every socket.                                                                                                                                                                                  | `-bs NUM`      |
| `--ip-limit NUM`              | The limit of same ip address connections.                                                                                                                                                                      | `-il NUM`      |
| `--ping-timeout NUM NUM`      | How long to wait in seconds and nanoseconds for a response/pong before disconnecting. NOTE: This should allow for the time of any non control frame operations that could occur especially any using timeouts. | `-pt NUM NUM`  |
| `--io-timeout NUM NUM`        | How long to wait in seconds and nanoseconds for a message before giving up and pinging.                                                                                                                        | `-it NUM NUM`  |
| `--pin-write-timeout NUM NUM` | The amount of time seconds and nanoseconds to lock a pin for after being written to.                                                                                                                           | `-pwt NUM NUM` |
| `--pin-mode-input NUM ...`    | The count of pins to set as input pins followed by a list of space separated pin numbers eg. "-pmi 4 15 12 3 4)". NOTE: The later occurring option will overwrite any duplicate pins from an earlier option.   | `-pmi NUM ...` |
| `--pin-mode-output NUM ...`   | The count of pins to set as output pins followed by a list of space separated pin numbers eg. "-pmo 3 16 10 1". NOTE: The later occurring option will overwrite any duplicate pins from an earlier option.     | `-pmo NUM ...` |